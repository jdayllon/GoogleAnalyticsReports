from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools

# Public Parameters
SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
DISCOVERY_URI = ('https://analyticsreporting.googleapis.com/$discovery/rest')

# Private Parameters
from ga_settings import KEY_FILE_LOCATION
from ga_settings import SERVICE_ACCOUNT_EMAIL
from ga_settings import VIEW_ID
from addict import Dict

# Pandas Dataframe
import pandas as pd


class GoogleAnalyticsReport:
    
    # API References: https://developers.google.com/analytics/devguides/reporting/core/dimsmets
    # Query Explorer: https://ga-dev-tools.appspot.com/query-explorer/

    # Internal objects
    _analytics = None
    _reports = []
    _viewId = None
    _pageToken = None
    _metrics = None
    _dimensions = None
    _orderBys = None
    _samplingLevel = None
    _response = None
    _responses = None
    _filters = None
    _body = None
    _rowCount = None


    # Safe Reporting
    __safe_reporting = 10
    # API Constants Metrics and Dimensions
    API = Dict()
    

    def __init__(self, **kwargs):
        
        for arg in kwargs:
            cur_internal_var_id =  "_" + arg
            if cur_internal_var_id in GoogleAnalyticsReport.__dict__:
                self.__dict__[cur_internal_var_id] = kwargs[arg]

        # Inits GA reporting object if doesn't come in inital parameters
        if self._analytics is None:
            self._analytics = self.initialize_analyticsreporting()

    def initialize_analyticsreporting(self):
        """Initializes an analyticsreporting service object.
        From HelloAnalytics.py (v4)
        Returns:
            analytics an authorized analyticsreporting service object.
        """

        credentials = ServiceAccountCredentials.from_p12_keyfile(
            SERVICE_ACCOUNT_EMAIL, KEY_FILE_LOCATION, scopes=SCOPES)

        http = credentials.authorize(httplib2.Http())

        # Build the service object.
        analytics = build('analytics', 'v4', http=http,
                          discoveryServiceUrl=DISCOVERY_URI)

        return analytics

    def get_report(self):
        # Use the Analytics Service Object to query the Analytics Reporting API V4.

        # Default Request
        body = {
            'reportRequests': [
                {
                    'viewId': VIEW_ID,
                    'dateRanges': [{'startDate': '7daysAgo', 'endDate': 'today'}],
                    'metrics': [{'expression': 'ga:pageviews'},
                                {"expression": "ga:sessions"},
                                {"expression": "ga:avgPageLoadTime"},
                                {"expression": "ga:serverResponseTime"},
                                ],
                    'dimensions': [{'name': 'ga:pagePath'}, {'name': 'ga:deviceCategory'}],
                    "orderBys":
                    [
                        {"fieldName": "ga:sessions", "sortOrder": "DESCENDING"},
                        {"fieldName": "ga:pageviews", "sortOrder": "DESCENDING"}
                    ],
                    "pageSize": 10000,
                }]
        }
        body_reportRequests = body['reportRequests'][0]

        if self._pageToken is not None:
            body_reportRequests['pageToken'] = str(self._pageToken)

        if self._metrics is not None:
            body_reportRequests['metrics'] = self._list_to_metrics(self._metrics)

        if self._dimensions is not None:
            body_reportRequests['dimensions'] = self._lst_to_dimensions(self._dimensions)

        if self._viewId is not None:
            # TODO: viewID to a list
            body_reportRequests['viewId'] = self._viewId

        if self._orderBys is not None:
            body_reportRequests['orderBys'] = self._lst_to_orderbys(self._orderBys)

        if self._samplingLevel is not None:
            body_reportRequests['samplingLevel'] = self._samplingLevel

        if self._filters is not None:
            body_reportRequests['filtersExpression'] = self._filters

        self._response = self._analytics.reports().batchGet(body=body).execute()
        return self._response

    def _get_pageToken(self, response=None):
        
        if response is None:
            response = self._response

        assert response is not None, "No response, please get it before"

        report = response['reports'][0]
        if 'nextPageToken' in report:
            return report['nextPageToken']
        else:
            return None
    
    def _get_rowCount(self, response=None):
        if response is None:
            response = self._response

        assert response is not None, "No response, please get it before"

        report = response['reports'][0]['data']
        if 'rowCount' in report:
            return report['rowCount']
        else:
            return None

    def _list_to_metrics(self, lst_metrics):
        #   Example:
        #  'metrics': [{'expression': 'ga:pageviews'},
        #              {"expression": "ga:sessions"},
        #              {"expression": "ga:avgPageLoadTime"},
        #              {"expression": "ga:serverResponseTime"},
        #             ]
        formatted_metrics = []
        for cur_metric in lst_metrics:
            formatted_metrics += [{'expression': cur_metric}]
        return formatted_metrics

    def _lst_to_dimensions(self, lst_dimensions):
        #   Example:
        # 'dimensions': [{'name': 'ga:pagePath'}, {'name':'ga:deviceCategory'}],
        formatted_dimensions = []
        for cur_dimension in lst_dimensions:
            formatted_dimensions += [{'name': cur_dimension}]
        return formatted_dimensions

    def _lst_to_orderbys(self, lst_orderbys):
        #   Call Examploe:
        #   _lst_to_orderbys([('ga:sessions','ASCENDING'),('ga:pageviews','DESCENDING') ])
        #   Example Output:
        # {"fieldName": "ga:sessions", "sortOrder": "DESCENDING"},
        # {"fieldName": "ga:pageviews", "sortOrder": "DESCENDING"}
        formatted_orderbys = []
        for cur_orderby in lst_orderbys:
            formatted_orderbys += [{'fieldName': cur_orderby[0],
                                    "sortOrder": cur_orderby[1]}]
        return formatted_orderbys

    def get_reports(self):
        # Gets all results and returns a list width reports

        self._responses = []
        pageToken = None

        while True:
            response = self.get_report()
            self._pageToken = self._get_pageToken(response)
            self._responses += [response]
            self._rowCount = self._get_rowCount(response)

            if self._pageToken is None:
                print("Finished!")
                break
            else:
                print("Loading %s results from %s (estimated)" % (self._pageToken,self._rowCount))
            # Safety break
            if len(self._responses) >= self.__safe_reporting:
                break

        return self._responses

    def get_report_df(self):
        # If there are responses generated it will return current responses in Dataframe
        # It there aren't responses it will throw a assert

        assert self._response is not None, "Any response collected from GA API"

        # Extracting info from reponse
        dimensions = self._response['reports'][0]['columnHeader']['dimensions']

        metrics = []
        metrics_types = []
        for cur_metric in self._response['reports'][0]['columnHeader']['metricHeader']['metricHeaderEntries']:
            metrics += [cur_metric['name']]
            metrics_types += [cur_metric['type']]

        responses_df = []
        for cur_response in self._responses:
            responses_df += [pd.DataFrame(cur_response['reports'][0]['data']['rows'])]
        df = pd.concat(responses_df)
        df = df.reset_index()
        df = df.drop('index',1)

        # Prepare Columns Metrics 
        df_metrics = df.metrics.apply(lambda x: pd.Series(x[0]['values']))
        df_metrics.columns = metrics

        ## Metrics type casting
        for pos_metric in range(0, len(metrics)):
            if metrics_types[pos_metric] == 'INTEGER':
                df_metrics[metrics[pos_metric]] = df_metrics[metrics[pos_metric]].apply(lambda x: int(x))
            elif metrics_types[pos_metric] == 'FLOAT':
                df_metrics[metrics[pos_metric]] = df_metrics[metrics[pos_metric]].apply(lambda x: float(x))
            # TODO: Check if there are another types of metrics

        # Prepare Columns Dimensios
        df_dimensions = df.dimensions.apply(pd.Series)
        df_dimensions.columns = dimensions

        df = pd.concat([df_dimensions,df_metrics],axis=1)
        
        return df


    # Parameters options

    API.SAMPLINGLEVEL.DEFAULT = "DEFAULT"
    API.SAMPLINGLEVEL.FASTER = "FASTER"
    API.SAMPLINGLEVEL.HIGHER_PRECISION = "HIGHER_PRECISION"

    # Dimensions and Metrics Google Analytics Api v4
 
    API.USER.DIMENSIONS.GA_USERTYPE="ga:userType"
    API.USER.DIMENSIONS.GA_SESSIONCOUNT="ga:sessionCount"
    API.USER.DIMENSIONS.GA_DAYSSINCELASTSESSION="ga:daysSinceLastSession"
    API.USER.DIMENSIONS.GA_USERDEFINEDVALUE="ga:userDefinedValue"
    API.USER.DIMENSIONS.GA_USERBUCKET="ga:userBucket"
    API.USER.DIMENSIONS.GA_VISITORTYPE="ga:visitorType"
    API.USER.DIMENSIONS.GA_VISITCOUNT="ga:visitCount"

    API.USER.METRICS.GA_USERS="ga:users"
    API.USER.METRICS.GA_NEWUSERS="ga:newUsers"
    API.USER.METRICS.GA_PERCENTNEWSESSIONS="ga:percentNewSessions"
    API.USER.METRICS.GA_1DAYUSERS="ga:1dayUsers"
    API.USER.METRICS.GA_7DAYUSERS="ga:7dayUsers"
    API.USER.METRICS.GA_14DAYUSERS="ga:14dayUsers"
    API.USER.METRICS.GA_30DAYUSERS="ga:30dayUsers"
    API.USER.METRICS.GA_SESSIONSPERUSER="ga:sessionsPerUser"
    API.USER.METRICS.GA_VISITORS="ga:visitors"
    API.USER.METRICS.GA_NEWVISITS="ga:newVisits"
    API.USER.METRICS.GA_PERCENTNEWVISITS="ga:percentNewVisits"


    API.SESSION.DIMENSIONS.GA_SESSIONDURATIONBUCKET="ga:sessionDurationBucket"
    API.SESSION.DIMENSIONS.GA_VISITLENGTH="ga:visitLength"

    API.SESSION.METRICS.GA_SESSIONS="ga:sessions"
    API.SESSION.METRICS.GA_BOUNCES="ga:bounces"
    API.SESSION.METRICS.GA_BOUNCERATE="ga:bounceRate"
    API.SESSION.METRICS.GA_SESSIONDURATION="ga:sessionDuration"
    API.SESSION.METRICS.GA_AVGSESSIONDURATION="ga:avgSessionDuration"
    API.SESSION.METRICS.GA_UNIQUEDIMENSIONCOMBINATIONS="ga:uniqueDimensionCombinations"
    API.SESSION.METRICS.GA_HITS="ga:hits"
    API.SESSION.METRICS.GA_VISITS="ga:visits"
    API.SESSION.METRICS.GA_VISITBOUNCERATE="ga:visitBounceRate"
    API.SESSION.METRICS.GA_TIMEONSITE="ga:timeOnSite"
    API.SESSION.METRICS.GA_AVGTIMEONSITE="ga:avgTimeOnSite"


    API.TRAFFICSOURCES.DIMENSIONS.GA_REFERRALPATH="ga:referralPath"
    API.TRAFFICSOURCES.DIMENSIONS.GA_FULLREFERRER="ga:fullReferrer"
    API.TRAFFICSOURCES.DIMENSIONS.GA_CAMPAIGN="ga:campaign"
    API.TRAFFICSOURCES.DIMENSIONS.GA_SOURCE="ga:source"
    API.TRAFFICSOURCES.DIMENSIONS.GA_MEDIUM="ga:medium"
    API.TRAFFICSOURCES.DIMENSIONS.GA_SOURCEMEDIUM="ga:sourceMedium"
    API.TRAFFICSOURCES.DIMENSIONS.GA_KEYWORD="ga:keyword"
    API.TRAFFICSOURCES.DIMENSIONS.GA_ADCONTENT="ga:adContent"
    API.TRAFFICSOURCES.DIMENSIONS.GA_SOCIALNETWORK="ga:socialNetwork"
    API.TRAFFICSOURCES.DIMENSIONS.GA_HASSOCIALSOURCEREFERRAL="ga:hasSocialSourceReferral"
    API.TRAFFICSOURCES.DIMENSIONS.GA_CAMPAIGNCODE="ga:campaignCode"




    API.ADWORDS.DIMENSIONS.GA_ADGROUP="ga:adGroup"
    API.ADWORDS.DIMENSIONS.GA_ADSLOT="ga:adSlot"
    API.ADWORDS.DIMENSIONS.GA_ADDISTRIBUTIONNETWORK="ga:adDistributionNetwork"
    API.ADWORDS.DIMENSIONS.GA_ADMATCHTYPE="ga:adMatchType"
    API.ADWORDS.DIMENSIONS.GA_ADKEYWORDMATCHTYPE="ga:adKeywordMatchType"
    API.ADWORDS.DIMENSIONS.GA_ADMATCHEDQUERY="ga:adMatchedQuery"
    API.ADWORDS.DIMENSIONS.GA_ADPLACEMENTDOMAIN="ga:adPlacementDomain"
    API.ADWORDS.DIMENSIONS.GA_ADPLACEMENTURL="ga:adPlacementUrl"
    API.ADWORDS.DIMENSIONS.GA_ADFORMAT="ga:adFormat"
    API.ADWORDS.DIMENSIONS.GA_ADTARGETINGTYPE="ga:adTargetingType"
    API.ADWORDS.DIMENSIONS.GA_ADTARGETINGOPTION="ga:adTargetingOption"
    API.ADWORDS.DIMENSIONS.GA_ADDISPLAYURL="ga:adDisplayUrl"
    API.ADWORDS.DIMENSIONS.GA_ADDESTINATIONURL="ga:adDestinationUrl"
    API.ADWORDS.DIMENSIONS.GA_ADWORDSCUSTOMERID="ga:adwordsCustomerID"
    API.ADWORDS.DIMENSIONS.GA_ADWORDSCAMPAIGNID="ga:adwordsCampaignID"
    API.ADWORDS.DIMENSIONS.GA_ADWORDSADGROUPID="ga:adwordsAdGroupID"
    API.ADWORDS.DIMENSIONS.GA_ADWORDSCREATIVEID="ga:adwordsCreativeID"
    API.ADWORDS.DIMENSIONS.GA_ADWORDSCRITERIAID="ga:adwordsCriteriaID"
    API.ADWORDS.DIMENSIONS.GA_ADQUERYWORDCOUNT="ga:adQueryWordCount"
    API.ADWORDS.DIMENSIONS.GA_ISTRUEVIEWVIDEOAD="ga:isTrueViewVideoAd"

    API.ADWORDS.METRICS.GA_IMPRESSIONS="ga:impressions"
    API.ADWORDS.METRICS.GA_ADCLICKS="ga:adClicks"
    API.ADWORDS.METRICS.GA_ADCOST="ga:adCost"
    API.ADWORDS.METRICS.GA_CPM="ga:CPM"
    API.ADWORDS.METRICS.GA_CPC="ga:CPC"
    API.ADWORDS.METRICS.GA_CTR="ga:CTR"
    API.ADWORDS.METRICS.GA_COSTPERTRANSACTION="ga:costPerTransaction"
    API.ADWORDS.METRICS.GA_COSTPERGOALCONVERSION="ga:costPerGoalConversion"
    API.ADWORDS.METRICS.GA_COSTPERCONVERSION="ga:costPerConversion"
    API.ADWORDS.METRICS.GA_RPC="ga:RPC"
    API.ADWORDS.METRICS.GA_ROAS="ga:ROAS"
    API.ADWORDS.METRICS.GA_ROI="ga:ROI"
    API.ADWORDS.METRICS.GA_MARGIN="ga:margin"


    API.GOALCONVERSIONS.DIMENSIONS.GA_GOALCOMPLETIONLOCATION="ga:goalCompletionLocation"
    API.GOALCONVERSIONS.DIMENSIONS.GA_GOALPREVIOUSSTEP1="ga:goalPreviousStep1"
    API.GOALCONVERSIONS.DIMENSIONS.GA_GOALPREVIOUSSTEP2="ga:goalPreviousStep2"
    API.GOALCONVERSIONS.DIMENSIONS.GA_GOALPREVIOUSSTEP3="ga:goalPreviousStep3"

    API.GOALCONVERSIONS.METRICS.GA_GOALXXSTARTS="ga:goalXXStarts"
    API.GOALCONVERSIONS.METRICS.GA_GOALSTARTSALL="ga:goalStartsAll"
    API.GOALCONVERSIONS.METRICS.GA_GOALXXCOMPLETIONS="ga:goalXXCompletions"
    API.GOALCONVERSIONS.METRICS.GA_GOALCOMPLETIONSALL="ga:goalCompletionsAll"
    API.GOALCONVERSIONS.METRICS.GA_GOALXXVALUE="ga:goalXXValue"
    API.GOALCONVERSIONS.METRICS.GA_GOALVALUEALL="ga:goalValueAll"
    API.GOALCONVERSIONS.METRICS.GA_GOALVALUEPERSESSION="ga:goalValuePerSession"
    API.GOALCONVERSIONS.METRICS.GA_GOALXXCONVERSIONRATE="ga:goalXXConversionRate"
    API.GOALCONVERSIONS.METRICS.GA_GOALCONVERSIONRATEALL="ga:goalConversionRateAll"
    API.GOALCONVERSIONS.METRICS.GA_GOALXXABANDONS="ga:goalXXAbandons"
    API.GOALCONVERSIONS.METRICS.GA_GOALABANDONSALL="ga:goalAbandonsAll"
    API.GOALCONVERSIONS.METRICS.GA_GOALXXABANDONRATE="ga:goalXXAbandonRate"
    API.GOALCONVERSIONS.METRICS.GA_GOALABANDONRATEALL="ga:goalAbandonRateAll"
    API.GOALCONVERSIONS.METRICS.GA_GOALVALUEPERVISIT="ga:goalValuePerVisit"


    API.PLATFORMORDEVICE.DIMENSIONS.GA_BROWSER="ga:browser"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_BROWSERVERSION="ga:browserVersion"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_OPERATINGSYSTEM="ga:operatingSystem"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_OPERATINGSYSTEMVERSION="ga:operatingSystemVersion"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_MOBILEDEVICEBRANDING="ga:mobileDeviceBranding"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_MOBILEDEVICEMODEL="ga:mobileDeviceModel"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_MOBILEINPUTSELECTOR="ga:mobileInputSelector"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_MOBILEDEVICEINFO="ga:mobileDeviceInfo"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_MOBILEDEVICEMARKETINGNAME="ga:mobileDeviceMarketingName"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_DEVICECATEGORY="ga:deviceCategory"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_BROWSERSIZE="ga:browserSize"
    API.PLATFORMORDEVICE.DIMENSIONS.GA_DATASOURCE="ga:dataSource"




    API.GEONETWORK.DIMENSIONS.GA_CONTINENT="ga:continent"
    API.GEONETWORK.DIMENSIONS.GA_SUBCONTINENT="ga:subContinent"
    API.GEONETWORK.DIMENSIONS.GA_COUNTRY="ga:country"
    API.GEONETWORK.DIMENSIONS.GA_REGION="ga:region"
    API.GEONETWORK.DIMENSIONS.GA_METRO="ga:metro"
    API.GEONETWORK.DIMENSIONS.GA_CITY="ga:city"
    API.GEONETWORK.DIMENSIONS.GA_LATITUDE="ga:latitude"
    API.GEONETWORK.DIMENSIONS.GA_LONGITUDE="ga:longitude"
    API.GEONETWORK.DIMENSIONS.GA_NETWORKDOMAIN="ga:networkDomain"
    API.GEONETWORK.DIMENSIONS.GA_NETWORKLOCATION="ga:networkLocation"
    API.GEONETWORK.DIMENSIONS.GA_CITYID="ga:cityId"
    API.GEONETWORK.DIMENSIONS.GA_CONTINENTID="ga:continentId"
    API.GEONETWORK.DIMENSIONS.GA_COUNTRYISOCODE="ga:countryIsoCode"
    API.GEONETWORK.DIMENSIONS.GA_METROID="ga:metroId"
    API.GEONETWORK.DIMENSIONS.GA_REGIONID="ga:regionId"
    API.GEONETWORK.DIMENSIONS.GA_REGIONISOCODE="ga:regionIsoCode"
    API.GEONETWORK.DIMENSIONS.GA_SUBCONTINENTCODE="ga:subContinentCode"




    API.SYSTEM.DIMENSIONS.GA_FLASHVERSION="ga:flashVersion"
    API.SYSTEM.DIMENSIONS.GA_JAVAENABLED="ga:javaEnabled"
    API.SYSTEM.DIMENSIONS.GA_LANGUAGE="ga:language"
    API.SYSTEM.DIMENSIONS.GA_SCREENCOLORS="ga:screenColors"
    API.SYSTEM.DIMENSIONS.GA_SOURCEPROPERTYDISPLAYNAME="ga:sourcePropertyDisplayName"
    API.SYSTEM.DIMENSIONS.GA_SOURCEPROPERTYTRACKINGID="ga:sourcePropertyTrackingId"
    API.SYSTEM.DIMENSIONS.GA_SCREENRESOLUTION="ga:screenResolution"




    API.SOCIALACTIVITIES.DIMENSIONS.GA_SOCIALACTIVITYCONTENTURL="ga:socialActivityContentUrl"




    API.PAGETRACKING.DIMENSIONS.GA_HOSTNAME="ga:hostname"
    API.PAGETRACKING.DIMENSIONS.GA_PAGEPATH="ga:pagePath"
    API.PAGETRACKING.DIMENSIONS.GA_PAGEPATHLEVEL1="ga:pagePathLevel1"
    API.PAGETRACKING.DIMENSIONS.GA_PAGEPATHLEVEL2="ga:pagePathLevel2"
    API.PAGETRACKING.DIMENSIONS.GA_PAGEPATHLEVEL3="ga:pagePathLevel3"
    API.PAGETRACKING.DIMENSIONS.GA_PAGEPATHLEVEL4="ga:pagePathLevel4"
    API.PAGETRACKING.DIMENSIONS.GA_PAGETITLE="ga:pageTitle"
    API.PAGETRACKING.DIMENSIONS.GA_LANDINGPAGEPATH="ga:landingPagePath"
    API.PAGETRACKING.DIMENSIONS.GA_SECONDPAGEPATH="ga:secondPagePath"
    API.PAGETRACKING.DIMENSIONS.GA_EXITPAGEPATH="ga:exitPagePath"
    API.PAGETRACKING.DIMENSIONS.GA_PREVIOUSPAGEPATH="ga:previousPagePath"
    API.PAGETRACKING.DIMENSIONS.GA_PAGEDEPTH="ga:pageDepth"

    API.PAGETRACKING.METRICS.GA_PAGEVALUE="ga:pageValue"
    API.PAGETRACKING.METRICS.GA_ENTRANCES="ga:entrances"
    API.PAGETRACKING.METRICS.GA_ENTRANCERATE="ga:entranceRate"
    API.PAGETRACKING.METRICS.GA_PAGEVIEWS="ga:pageviews"
    API.PAGETRACKING.METRICS.GA_PAGEVIEWSPERSESSION="ga:pageviewsPerSession"
    API.PAGETRACKING.METRICS.GA_UNIQUEPAGEVIEWS="ga:uniquePageviews"
    API.PAGETRACKING.METRICS.GA_TIMEONPAGE="ga:timeOnPage"
    API.PAGETRACKING.METRICS.GA_AVGTIMEONPAGE="ga:avgTimeOnPage"
    API.PAGETRACKING.METRICS.GA_EXITS="ga:exits"
    API.PAGETRACKING.METRICS.GA_EXITRATE="ga:exitRate"
    API.PAGETRACKING.METRICS.GA_PAGEVIEWSPERVISIT="ga:pageviewsPerVisit"


    API.CONTENTGROUPING.DIMENSIONS.GA_LANDINGCONTENTGROUPXX="ga:landingContentGroupXX"
    API.CONTENTGROUPING.DIMENSIONS.GA_PREVIOUSCONTENTGROUPXX="ga:previousContentGroupXX"
    API.CONTENTGROUPING.DIMENSIONS.GA_CONTENTGROUPXX="ga:contentGroupXX"




    API.INTERNALSEARCH.DIMENSIONS.GA_SEARCHUSED="ga:searchUsed"
    API.INTERNALSEARCH.DIMENSIONS.GA_SEARCHKEYWORD="ga:searchKeyword"
    API.INTERNALSEARCH.DIMENSIONS.GA_SEARCHKEYWORDREFINEMENT="ga:searchKeywordRefinement"
    API.INTERNALSEARCH.DIMENSIONS.GA_SEARCHCATEGORY="ga:searchCategory"
    API.INTERNALSEARCH.DIMENSIONS.GA_SEARCHSTARTPAGE="ga:searchStartPage"
    API.INTERNALSEARCH.DIMENSIONS.GA_SEARCHDESTINATIONPAGE="ga:searchDestinationPage"
    API.INTERNALSEARCH.DIMENSIONS.GA_SEARCHAFTERDESTINATIONPAGE="ga:searchAfterDestinationPage"

    API.INTERNALSEARCH.METRICS.GA_SEARCHRESULTVIEWS="ga:searchResultViews"
    API.INTERNALSEARCH.METRICS.GA_SEARCHUNIQUES="ga:searchUniques"
    API.INTERNALSEARCH.METRICS.GA_AVGSEARCHRESULTVIEWS="ga:avgSearchResultViews"
    API.INTERNALSEARCH.METRICS.GA_SEARCHSESSIONS="ga:searchSessions"
    API.INTERNALSEARCH.METRICS.GA_PERCENTSESSIONSWITHSEARCH="ga:percentSessionsWithSearch"
    API.INTERNALSEARCH.METRICS.GA_SEARCHDEPTH="ga:searchDepth"
    API.INTERNALSEARCH.METRICS.GA_AVGSEARCHDEPTH="ga:avgSearchDepth"
    API.INTERNALSEARCH.METRICS.GA_SEARCHREFINEMENTS="ga:searchRefinements"
    API.INTERNALSEARCH.METRICS.GA_PERCENTSEARCHREFINEMENTS="ga:percentSearchRefinements"
    API.INTERNALSEARCH.METRICS.GA_SEARCHDURATION="ga:searchDuration"
    API.INTERNALSEARCH.METRICS.GA_AVGSEARCHDURATION="ga:avgSearchDuration"
    API.INTERNALSEARCH.METRICS.GA_SEARCHEXITS="ga:searchExits"
    API.INTERNALSEARCH.METRICS.GA_SEARCHEXITRATE="ga:searchExitRate"
    API.INTERNALSEARCH.METRICS.GA_SEARCHGOALXXCONVERSIONRATE="ga:searchGoalXXConversionRate"
    API.INTERNALSEARCH.METRICS.GA_SEARCHGOALCONVERSIONRATEALL="ga:searchGoalConversionRateAll"
    API.INTERNALSEARCH.METRICS.GA_GOALVALUEALLPERSEARCH="ga:goalValueAllPerSearch"
    API.INTERNALSEARCH.METRICS.GA_SEARCHVISITS="ga:searchVisits"
    API.INTERNALSEARCH.METRICS.GA_PERCENTVISITSWITHSEARCH="ga:percentVisitsWithSearch"




    API.SITESPEED.METRICS.GA_PAGELOADTIME="ga:pageLoadTime"
    API.SITESPEED.METRICS.GA_PAGELOADSAMPLE="ga:pageLoadSample"
    API.SITESPEED.METRICS.GA_AVGPAGELOADTIME="ga:avgPageLoadTime"
    API.SITESPEED.METRICS.GA_DOMAINLOOKUPTIME="ga:domainLookupTime"
    API.SITESPEED.METRICS.GA_AVGDOMAINLOOKUPTIME="ga:avgDomainLookupTime"
    API.SITESPEED.METRICS.GA_PAGEDOWNLOADTIME="ga:pageDownloadTime"
    API.SITESPEED.METRICS.GA_AVGPAGEDOWNLOADTIME="ga:avgPageDownloadTime"
    API.SITESPEED.METRICS.GA_REDIRECTIONTIME="ga:redirectionTime"
    API.SITESPEED.METRICS.GA_AVGREDIRECTIONTIME="ga:avgRedirectionTime"
    API.SITESPEED.METRICS.GA_SERVERCONNECTIONTIME="ga:serverConnectionTime"
    API.SITESPEED.METRICS.GA_AVGSERVERCONNECTIONTIME="ga:avgServerConnectionTime"
    API.SITESPEED.METRICS.GA_SERVERRESPONSETIME="ga:serverResponseTime"
    API.SITESPEED.METRICS.GA_AVGSERVERRESPONSETIME="ga:avgServerResponseTime"
    API.SITESPEED.METRICS.GA_SPEEDMETRICSSAMPLE="ga:speedMetricsSample"
    API.SITESPEED.METRICS.GA_DOMINTERACTIVETIME="ga:domInteractiveTime"
    API.SITESPEED.METRICS.GA_AVGDOMINTERACTIVETIME="ga:avgDomInteractiveTime"
    API.SITESPEED.METRICS.GA_DOMCONTENTLOADEDTIME="ga:domContentLoadedTime"
    API.SITESPEED.METRICS.GA_AVGDOMCONTENTLOADEDTIME="ga:avgDomContentLoadedTime"
    API.SITESPEED.METRICS.GA_DOMLATENCYMETRICSSAMPLE="ga:domLatencyMetricsSample"


    API.APPTRACKING.DIMENSIONS.GA_APPINSTALLERID="ga:appInstallerId"
    API.APPTRACKING.DIMENSIONS.GA_APPVERSION="ga:appVersion"
    API.APPTRACKING.DIMENSIONS.GA_APPNAME="ga:appName"
    API.APPTRACKING.DIMENSIONS.GA_APPID="ga:appId"
    API.APPTRACKING.DIMENSIONS.GA_SCREENNAME="ga:screenName"
    API.APPTRACKING.DIMENSIONS.GA_SCREENDEPTH="ga:screenDepth"
    API.APPTRACKING.DIMENSIONS.GA_LANDINGSCREENNAME="ga:landingScreenName"
    API.APPTRACKING.DIMENSIONS.GA_EXITSCREENNAME="ga:exitScreenName"

    API.APPTRACKING.METRICS.GA_SCREENVIEWS="ga:screenviews"
    API.APPTRACKING.METRICS.GA_UNIQUESCREENVIEWS="ga:uniqueScreenviews"
    API.APPTRACKING.METRICS.GA_SCREENVIEWSPERSESSION="ga:screenviewsPerSession"
    API.APPTRACKING.METRICS.GA_TIMEONSCREEN="ga:timeOnScreen"
    API.APPTRACKING.METRICS.GA_AVGSCREENVIEWDURATION="ga:avgScreenviewDuration"
    API.APPTRACKING.METRICS.GA_UNIQUEAPPVIEWS="ga:uniqueAppviews"


    API.EVENTTRACKING.DIMENSIONS.GA_EVENTCATEGORY="ga:eventCategory"
    API.EVENTTRACKING.DIMENSIONS.GA_EVENTACTION="ga:eventAction"
    API.EVENTTRACKING.DIMENSIONS.GA_EVENTLABEL="ga:eventLabel"

    API.EVENTTRACKING.METRICS.GA_TOTALEVENTS="ga:totalEvents"
    API.EVENTTRACKING.METRICS.GA_UNIQUEEVENTS="ga:uniqueEvents"
    API.EVENTTRACKING.METRICS.GA_EVENTVALUE="ga:eventValue"
    API.EVENTTRACKING.METRICS.GA_AVGEVENTVALUE="ga:avgEventValue"
    API.EVENTTRACKING.METRICS.GA_SESSIONSWITHEVENT="ga:sessionsWithEvent"
    API.EVENTTRACKING.METRICS.GA_EVENTSPERSESSIONWITHEVENT="ga:eventsPerSessionWithEvent"
    API.EVENTTRACKING.METRICS.GA_VISITSWITHEVENT="ga:visitsWithEvent"
    API.EVENTTRACKING.METRICS.GA_EVENTSPERVISITWITHEVENT="ga:eventsPerVisitWithEvent"


    API.ECOMMERCE.DIMENSIONS.GA_TRANSACTIONID="ga:transactionId"
    API.ECOMMERCE.DIMENSIONS.GA_AFFILIATION="ga:affiliation"
    API.ECOMMERCE.DIMENSIONS.GA_SESSIONSTOTRANSACTION="ga:sessionsToTransaction"
    API.ECOMMERCE.DIMENSIONS.GA_DAYSTOTRANSACTION="ga:daysToTransaction"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTSKU="ga:productSku"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTNAME="ga:productName"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTCATEGORY="ga:productCategory"
    API.ECOMMERCE.DIMENSIONS.GA_CURRENCYCODE="ga:currencyCode"
    API.ECOMMERCE.DIMENSIONS.GA_CHECKOUTOPTIONS="ga:checkoutOptions"
    API.ECOMMERCE.DIMENSIONS.GA_INTERNALPROMOTIONCREATIVE="ga:internalPromotionCreative"
    API.ECOMMERCE.DIMENSIONS.GA_INTERNALPROMOTIONID="ga:internalPromotionId"
    API.ECOMMERCE.DIMENSIONS.GA_INTERNALPROMOTIONNAME="ga:internalPromotionName"
    API.ECOMMERCE.DIMENSIONS.GA_INTERNALPROMOTIONPOSITION="ga:internalPromotionPosition"
    API.ECOMMERCE.DIMENSIONS.GA_ORDERCOUPONCODE="ga:orderCouponCode"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTBRAND="ga:productBrand"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTCATEGORYHIERARCHY="ga:productCategoryHierarchy"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTCATEGORYLEVELXX="ga:productCategoryLevelXX"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTCOUPONCODE="ga:productCouponCode"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTLISTNAME="ga:productListName"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTLISTPOSITION="ga:productListPosition"
    API.ECOMMERCE.DIMENSIONS.GA_PRODUCTVARIANT="ga:productVariant"
    API.ECOMMERCE.DIMENSIONS.GA_SHOPPINGSTAGE="ga:shoppingStage"
    API.ECOMMERCE.DIMENSIONS.GA_VISITSTOTRANSACTION="ga:visitsToTransaction"

    API.ECOMMERCE.METRICS.GA_TRANSACTIONS="ga:transactions"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONSPERSESSION="ga:transactionsPerSession"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONREVENUE="ga:transactionRevenue"
    API.ECOMMERCE.METRICS.GA_REVENUEPERTRANSACTION="ga:revenuePerTransaction"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONREVENUEPERSESSION="ga:transactionRevenuePerSession"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONSHIPPING="ga:transactionShipping"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONTAX="ga:transactionTax"
    API.ECOMMERCE.METRICS.GA_TOTALVALUE="ga:totalValue"
    API.ECOMMERCE.METRICS.GA_ITEMQUANTITY="ga:itemQuantity"
    API.ECOMMERCE.METRICS.GA_UNIQUEPURCHASES="ga:uniquePurchases"
    API.ECOMMERCE.METRICS.GA_REVENUEPERITEM="ga:revenuePerItem"
    API.ECOMMERCE.METRICS.GA_ITEMREVENUE="ga:itemRevenue"
    API.ECOMMERCE.METRICS.GA_ITEMSPERPURCHASE="ga:itemsPerPurchase"
    API.ECOMMERCE.METRICS.GA_LOCALTRANSACTIONREVENUE="ga:localTransactionRevenue"
    API.ECOMMERCE.METRICS.GA_LOCALTRANSACTIONSHIPPING="ga:localTransactionShipping"
    API.ECOMMERCE.METRICS.GA_LOCALTRANSACTIONTAX="ga:localTransactionTax"
    API.ECOMMERCE.METRICS.GA_LOCALITEMREVENUE="ga:localItemRevenue"
    API.ECOMMERCE.METRICS.GA_BUYTODETAILRATE="ga:buyToDetailRate"
    API.ECOMMERCE.METRICS.GA_CARTTODETAILRATE="ga:cartToDetailRate"
    API.ECOMMERCE.METRICS.GA_INTERNALPROMOTIONCTR="ga:internalPromotionCTR"
    API.ECOMMERCE.METRICS.GA_INTERNALPROMOTIONCLICKS="ga:internalPromotionClicks"
    API.ECOMMERCE.METRICS.GA_INTERNALPROMOTIONVIEWS="ga:internalPromotionViews"
    API.ECOMMERCE.METRICS.GA_LOCALPRODUCTREFUNDAMOUNT="ga:localProductRefundAmount"
    API.ECOMMERCE.METRICS.GA_LOCALREFUNDAMOUNT="ga:localRefundAmount"
    API.ECOMMERCE.METRICS.GA_PRODUCTADDSTOCART="ga:productAddsToCart"
    API.ECOMMERCE.METRICS.GA_PRODUCTCHECKOUTS="ga:productCheckouts"
    API.ECOMMERCE.METRICS.GA_PRODUCTDETAILVIEWS="ga:productDetailViews"
    API.ECOMMERCE.METRICS.GA_PRODUCTLISTCTR="ga:productListCTR"
    API.ECOMMERCE.METRICS.GA_PRODUCTLISTCLICKS="ga:productListClicks"
    API.ECOMMERCE.METRICS.GA_PRODUCTLISTVIEWS="ga:productListViews"
    API.ECOMMERCE.METRICS.GA_PRODUCTREFUNDAMOUNT="ga:productRefundAmount"
    API.ECOMMERCE.METRICS.GA_PRODUCTREFUNDS="ga:productRefunds"
    API.ECOMMERCE.METRICS.GA_PRODUCTREMOVESFROMCART="ga:productRemovesFromCart"
    API.ECOMMERCE.METRICS.GA_PRODUCTREVENUEPERPURCHASE="ga:productRevenuePerPurchase"
    API.ECOMMERCE.METRICS.GA_QUANTITYADDEDTOCART="ga:quantityAddedToCart"
    API.ECOMMERCE.METRICS.GA_QUANTITYCHECKEDOUT="ga:quantityCheckedOut"
    API.ECOMMERCE.METRICS.GA_QUANTITYREFUNDED="ga:quantityRefunded"
    API.ECOMMERCE.METRICS.GA_QUANTITYREMOVEDFROMCART="ga:quantityRemovedFromCart"
    API.ECOMMERCE.METRICS.GA_REFUNDAMOUNT="ga:refundAmount"
    API.ECOMMERCE.METRICS.GA_REVENUEPERUSER="ga:revenuePerUser"
    API.ECOMMERCE.METRICS.GA_TOTALREFUNDS="ga:totalRefunds"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONSPERUSER="ga:transactionsPerUser"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONSPERVISIT="ga:transactionsPerVisit"
    API.ECOMMERCE.METRICS.GA_TRANSACTIONREVENUEPERVISIT="ga:transactionRevenuePerVisit"


    API.SOCIALINTERACTIONS.DIMENSIONS.GA_SOCIALINTERACTIONNETWORK="ga:socialInteractionNetwork"
    API.SOCIALINTERACTIONS.DIMENSIONS.GA_SOCIALINTERACTIONACTION="ga:socialInteractionAction"
    API.SOCIALINTERACTIONS.DIMENSIONS.GA_SOCIALINTERACTIONNETWORKACTION="ga:socialInteractionNetworkAction"
    API.SOCIALINTERACTIONS.DIMENSIONS.GA_SOCIALINTERACTIONTARGET="ga:socialInteractionTarget"
    API.SOCIALINTERACTIONS.DIMENSIONS.GA_SOCIALENGAGEMENTTYPE="ga:socialEngagementType"

    API.SOCIALINTERACTIONS.METRICS.GA_SOCIALINTERACTIONS="ga:socialInteractions"
    API.SOCIALINTERACTIONS.METRICS.GA_UNIQUESOCIALINTERACTIONS="ga:uniqueSocialInteractions"
    API.SOCIALINTERACTIONS.METRICS.GA_SOCIALINTERACTIONSPERSESSION="ga:socialInteractionsPerSession"
    API.SOCIALINTERACTIONS.METRICS.GA_SOCIALINTERACTIONSPERVISIT="ga:socialInteractionsPerVisit"


    API.USERTIMINGS.DIMENSIONS.GA_USERTIMINGCATEGORY="ga:userTimingCategory"
    API.USERTIMINGS.DIMENSIONS.GA_USERTIMINGLABEL="ga:userTimingLabel"
    API.USERTIMINGS.DIMENSIONS.GA_USERTIMINGVARIABLE="ga:userTimingVariable"

    API.USERTIMINGS.METRICS.GA_USERTIMINGVALUE="ga:userTimingValue"
    API.USERTIMINGS.METRICS.GA_USERTIMINGSAMPLE="ga:userTimingSample"
    API.USERTIMINGS.METRICS.GA_AVGUSERTIMINGVALUE="ga:avgUserTimingValue"


    API.EXCEPTIONS.DIMENSIONS.GA_EXCEPTIONDESCRIPTION="ga:exceptionDescription"

    API.EXCEPTIONS.METRICS.GA_EXCEPTIONS="ga:exceptions"
    API.EXCEPTIONS.METRICS.GA_EXCEPTIONSPERSCREENVIEW="ga:exceptionsPerScreenview"
    API.EXCEPTIONS.METRICS.GA_FATALEXCEPTIONS="ga:fatalExceptions"
    API.EXCEPTIONS.METRICS.GA_FATALEXCEPTIONSPERSCREENVIEW="ga:fatalExceptionsPerScreenview"


    API.CONTENTEXPERIMENTS.DIMENSIONS.GA_EXPERIMENTID="ga:experimentId"
    API.CONTENTEXPERIMENTS.DIMENSIONS.GA_EXPERIMENTVARIANT="ga:experimentVariant"




    API.CUSTOMVARIABLESORCOLUMNS.DIMENSIONS.GA_DIMENSIONXX="ga:dimensionXX"
    API.CUSTOMVARIABLESORCOLUMNS.DIMENSIONS.GA_CUSTOMVARNAMEXX="ga:customVarNameXX"
    API.CUSTOMVARIABLESORCOLUMNS.DIMENSIONS.GA_CUSTOMVARVALUEXX="ga:customVarValueXX"

    API.CUSTOMVARIABLESORCOLUMNS.METRICS.GA_METRICXX="ga:metricXX"
    API.CUSTOMVARIABLESORCOLUMNS.METRICS.GA_CALCMETRIC_="ga:calcMetric_"


    API.TIME.DIMENSIONS.GA_DATE="ga:date"
    API.TIME.DIMENSIONS.GA_YEAR="ga:year"
    API.TIME.DIMENSIONS.GA_MONTH="ga:month"
    API.TIME.DIMENSIONS.GA_WEEK="ga:week"
    API.TIME.DIMENSIONS.GA_DAY="ga:day"
    API.TIME.DIMENSIONS.GA_HOUR="ga:hour"
    API.TIME.DIMENSIONS.GA_MINUTE="ga:minute"
    API.TIME.DIMENSIONS.GA_NTHMONTH="ga:nthMonth"
    API.TIME.DIMENSIONS.GA_NTHWEEK="ga:nthWeek"
    API.TIME.DIMENSIONS.GA_NTHDAY="ga:nthDay"
    API.TIME.DIMENSIONS.GA_NTHMINUTE="ga:nthMinute"
    API.TIME.DIMENSIONS.GA_DAYOFWEEK="ga:dayOfWeek"
    API.TIME.DIMENSIONS.GA_DAYOFWEEKNAME="ga:dayOfWeekName"
    API.TIME.DIMENSIONS.GA_DATEHOUR="ga:dateHour"
    API.TIME.DIMENSIONS.GA_DATEHOURMINUTE="ga:dateHourMinute"
    API.TIME.DIMENSIONS.GA_YEARMONTH="ga:yearMonth"
    API.TIME.DIMENSIONS.GA_YEARWEEK="ga:yearWeek"
    API.TIME.DIMENSIONS.GA_ISOWEEK="ga:isoWeek"
    API.TIME.DIMENSIONS.GA_ISOYEAR="ga:isoYear"
    API.TIME.DIMENSIONS.GA_ISOYEARISOWEEK="ga:isoYearIsoWeek"
    API.TIME.DIMENSIONS.GA_NTHHOUR="ga:nthHour"




    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKAD="ga:dcmClickAd"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKADID="ga:dcmClickAdId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKADTYPE="ga:dcmClickAdType"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKADTYPEID="ga:dcmClickAdTypeId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKADVERTISER="ga:dcmClickAdvertiser"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKADVERTISERID="ga:dcmClickAdvertiserId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKCAMPAIGN="ga:dcmClickCampaign"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKCAMPAIGNID="ga:dcmClickCampaignId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKCREATIVEID="ga:dcmClickCreativeId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKCREATIVE="ga:dcmClickCreative"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKRENDERINGID="ga:dcmClickRenderingId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKCREATIVETYPE="ga:dcmClickCreativeType"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKCREATIVETYPEID="ga:dcmClickCreativeTypeId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKCREATIVEVERSION="ga:dcmClickCreativeVersion"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKSITE="ga:dcmClickSite"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKSITEID="ga:dcmClickSiteId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKSITEPLACEMENT="ga:dcmClickSitePlacement"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKSITEPLACEMENTID="ga:dcmClickSitePlacementId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMCLICKSPOTID="ga:dcmClickSpotId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMFLOODLIGHTACTIVITY="ga:dcmFloodlightActivity"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMFLOODLIGHTACTIVITYANDGROUP="ga:dcmFloodlightActivityAndGroup"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMFLOODLIGHTACTIVITYGROUP="ga:dcmFloodlightActivityGroup"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMFLOODLIGHTACTIVITYGROUPID="ga:dcmFloodlightActivityGroupId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMFLOODLIGHTACTIVITYID="ga:dcmFloodlightActivityId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMFLOODLIGHTADVERTISERID="ga:dcmFloodlightAdvertiserId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMFLOODLIGHTSPOTID="ga:dcmFloodlightSpotId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTAD="ga:dcmLastEventAd"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTADID="ga:dcmLastEventAdId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTADTYPE="ga:dcmLastEventAdType"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTADTYPEID="ga:dcmLastEventAdTypeId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTADVERTISER="ga:dcmLastEventAdvertiser"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTADVERTISERID="ga:dcmLastEventAdvertiserId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTATTRIBUTIONTYPE="ga:dcmLastEventAttributionType"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTCAMPAIGN="ga:dcmLastEventCampaign"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTCAMPAIGNID="ga:dcmLastEventCampaignId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTCREATIVEID="ga:dcmLastEventCreativeId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTCREATIVE="ga:dcmLastEventCreative"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTRENDERINGID="ga:dcmLastEventRenderingId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTCREATIVETYPE="ga:dcmLastEventCreativeType"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTCREATIVETYPEID="ga:dcmLastEventCreativeTypeId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTCREATIVEVERSION="ga:dcmLastEventCreativeVersion"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTSITE="ga:dcmLastEventSite"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTSITEID="ga:dcmLastEventSiteId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTSITEPLACEMENT="ga:dcmLastEventSitePlacement"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTSITEPLACEMENTID="ga:dcmLastEventSitePlacementId"
    API.DOUBLECLICKCAMPAIGNMANAGER.DIMENSIONS.GA_DCMLASTEVENTSPOTID="ga:dcmLastEventSpotId"

    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMFLOODLIGHTQUANTITY="ga:dcmFloodlightQuantity"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMFLOODLIGHTREVENUE="ga:dcmFloodlightRevenue"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMCPC="ga:dcmCPC"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMCTR="ga:dcmCTR"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMCLICKS="ga:dcmClicks"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMCOST="ga:dcmCost"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMIMPRESSIONS="ga:dcmImpressions"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMROAS="ga:dcmROAS"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMRPC="ga:dcmRPC"
    API.DOUBLECLICKCAMPAIGNMANAGER.METRICS.GA_DCMMARGIN="ga:dcmMargin"


    API.AUDIENCE.DIMENSIONS.GA_USERAGEBRACKET="ga:userAgeBracket"
    API.AUDIENCE.DIMENSIONS.GA_USERGENDER="ga:userGender"
    API.AUDIENCE.DIMENSIONS.GA_INTERESTOTHERCATEGORY="ga:interestOtherCategory"
    API.AUDIENCE.DIMENSIONS.GA_INTERESTAFFINITYCATEGORY="ga:interestAffinityCategory"
    API.AUDIENCE.DIMENSIONS.GA_INTERESTINMARKETCATEGORY="ga:interestInMarketCategory"
    API.AUDIENCE.DIMENSIONS.GA_VISITORAGEBRACKET="ga:visitorAgeBracket"
    API.AUDIENCE.DIMENSIONS.GA_VISITORGENDER="ga:visitorGender"






    API.ADSENSE.METRICS.GA_ADSENSEREVENUE="ga:adsenseRevenue"
    API.ADSENSE.METRICS.GA_ADSENSEADUNITSVIEWED="ga:adsenseAdUnitsViewed"
    API.ADSENSE.METRICS.GA_ADSENSEADSVIEWED="ga:adsenseAdsViewed"
    API.ADSENSE.METRICS.GA_ADSENSEADSCLICKS="ga:adsenseAdsClicks"
    API.ADSENSE.METRICS.GA_ADSENSEPAGEIMPRESSIONS="ga:adsensePageImpressions"
    API.ADSENSE.METRICS.GA_ADSENSECTR="ga:adsenseCTR"
    API.ADSENSE.METRICS.GA_ADSENSEECPM="ga:adsenseECPM"
    API.ADSENSE.METRICS.GA_ADSENSEEXITS="ga:adsenseExits"
    API.ADSENSE.METRICS.GA_ADSENSEVIEWABLEIMPRESSIONPERCENT="ga:adsenseViewableImpressionPercent"
    API.ADSENSE.METRICS.GA_ADSENSECOVERAGE="ga:adsenseCoverage"




    API.ADEXCHANGE.METRICS.GA_ADXIMPRESSIONS="ga:adxImpressions"
    API.ADEXCHANGE.METRICS.GA_ADXCOVERAGE="ga:adxCoverage"
    API.ADEXCHANGE.METRICS.GA_ADXMONETIZEDPAGEVIEWS="ga:adxMonetizedPageviews"
    API.ADEXCHANGE.METRICS.GA_ADXIMPRESSIONSPERSESSION="ga:adxImpressionsPerSession"
    API.ADEXCHANGE.METRICS.GA_ADXVIEWABLEIMPRESSIONSPERCENT="ga:adxViewableImpressionsPercent"
    API.ADEXCHANGE.METRICS.GA_ADXCLICKS="ga:adxClicks"
    API.ADEXCHANGE.METRICS.GA_ADXCTR="ga:adxCTR"
    API.ADEXCHANGE.METRICS.GA_ADXREVENUE="ga:adxRevenue"
    API.ADEXCHANGE.METRICS.GA_ADXREVENUEPER1000SESSIONS="ga:adxRevenuePer1000Sessions"
    API.ADEXCHANGE.METRICS.GA_ADXECPM="ga:adxECPM"




    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPIMPRESSIONS="ga:dfpImpressions"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPCOVERAGE="ga:dfpCoverage"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPMONETIZEDPAGEVIEWS="ga:dfpMonetizedPageviews"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPIMPRESSIONSPERSESSION="ga:dfpImpressionsPerSession"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPVIEWABLEIMPRESSIONSPERCENT="ga:dfpViewableImpressionsPercent"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPCLICKS="ga:dfpClicks"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPCTR="ga:dfpCTR"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPREVENUE="ga:dfpRevenue"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPREVENUEPER1000SESSIONS="ga:dfpRevenuePer1000Sessions"
    API.DOUBLECLICKFORPUBLISHERS.METRICS.GA_DFPECPM="ga:dfpECPM"




    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLIMPRESSIONS="ga:backfillImpressions"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLCOVERAGE="ga:backfillCoverage"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLMONETIZEDPAGEVIEWS="ga:backfillMonetizedPageviews"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLIMPRESSIONSPERSESSION="ga:backfillImpressionsPerSession"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLVIEWABLEIMPRESSIONSPERCENT="ga:backfillViewableImpressionsPercent"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLCLICKS="ga:backfillClicks"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLCTR="ga:backfillCTR"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLREVENUE="ga:backfillRevenue"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLREVENUEPER1000SESSIONS="ga:backfillRevenuePer1000Sessions"
    API.DOUBLECLICKFORPUBLISHERSBACKFILL.METRICS.GA_BACKFILLECPM="ga:backfillECPM"


    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_ACQUISITIONCAMPAIGN="ga:acquisitionCampaign"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_ACQUISITIONMEDIUM="ga:acquisitionMedium"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_ACQUISITIONSOURCE="ga:acquisitionSource"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_ACQUISITIONSOURCEMEDIUM="ga:acquisitionSourceMedium"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_ACQUISITIONTRAFFICCHANNEL="ga:acquisitionTrafficChannel"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_COHORT="ga:cohort"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_COHORTNTHDAY="ga:cohortNthDay"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_COHORTNTHMONTH="ga:cohortNthMonth"
    API.LIFETIMEVALUEANDCOHORTS.DIMENSIONS.GA_COHORTNTHWEEK="ga:cohortNthWeek"

    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTACTIVEUSERS="ga:cohortActiveUsers"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTAPPVIEWSPERUSER="ga:cohortAppviewsPerUser"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTAPPVIEWSPERUSERWITHLIFETIMECRITERIA="ga:cohortAppviewsPerUserWithLifetimeCriteria"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTGOALCOMPLETIONSPERUSER="ga:cohortGoalCompletionsPerUser"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTGOALCOMPLETIONSPERUSERWITHLIFETIMECRITERIA="ga:cohortGoalCompletionsPerUserWithLifetimeCriteria"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTPAGEVIEWSPERUSER="ga:cohortPageviewsPerUser"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTPAGEVIEWSPERUSERWITHLIFETIMECRITERIA="ga:cohortPageviewsPerUserWithLifetimeCriteria"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTRETENTIONRATE="ga:cohortRetentionRate"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTREVENUEPERUSER="ga:cohortRevenuePerUser"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTREVENUEPERUSERWITHLIFETIMECRITERIA="ga:cohortRevenuePerUserWithLifetimeCriteria"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTSESSIONDURATIONPERUSER="ga:cohortSessionDurationPerUser"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTSESSIONDURATIONPERUSERWITHLIFETIMECRITERIA="ga:cohortSessionDurationPerUserWithLifetimeCriteria"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTSESSIONSPERUSER="ga:cohortSessionsPerUser"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTSESSIONSPERUSERWITHLIFETIMECRITERIA="ga:cohortSessionsPerUserWithLifetimeCriteria"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTTOTALUSERS="ga:cohortTotalUsers"
    API.LIFETIMEVALUEANDCOHORTS.METRICS.GA_COHORTTOTALUSERSWITHLIFETIMECRITERIA="ga:cohortTotalUsersWithLifetimeCriteria"


    API.CHANNELGROUPING.DIMENSIONS.GA_CHANNELGROUPING="ga:channelGrouping"




    API.RELATEDPRODUCTS.DIMENSIONS.GA_CORRELATIONMODELID="ga:correlationModelId"
    API.RELATEDPRODUCTS.DIMENSIONS.GA_QUERYPRODUCTID="ga:queryProductId"
    API.RELATEDPRODUCTS.DIMENSIONS.GA_QUERYPRODUCTNAME="ga:queryProductName"
    API.RELATEDPRODUCTS.DIMENSIONS.GA_QUERYPRODUCTVARIATION="ga:queryProductVariation"
    API.RELATEDPRODUCTS.DIMENSIONS.GA_RELATEDPRODUCTID="ga:relatedProductId"
    API.RELATEDPRODUCTS.DIMENSIONS.GA_RELATEDPRODUCTNAME="ga:relatedProductName"
    API.RELATEDPRODUCTS.DIMENSIONS.GA_RELATEDPRODUCTVARIATION="ga:relatedProductVariation"

    API.RELATEDPRODUCTS.METRICS.GA_CORRELATIONSCORE="ga:correlationScore"
    API.RELATEDPRODUCTS.METRICS.GA_QUERYPRODUCTQUANTITY="ga:queryProductQuantity"
    API.RELATEDPRODUCTS.METRICS.GA_RELATEDPRODUCTQUANTITY="ga:relatedProductQuantity"


    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKADVERTISER="ga:dbmClickAdvertiser"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKADVERTISERID="ga:dbmClickAdvertiserId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKCREATIVEID="ga:dbmClickCreativeId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKEXCHANGE="ga:dbmClickExchange"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKEXCHANGEID="ga:dbmClickExchangeId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKINSERTIONORDER="ga:dbmClickInsertionOrder"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKINSERTIONORDERID="ga:dbmClickInsertionOrderId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKLINEITEM="ga:dbmClickLineItem"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKLINEITEMID="ga:dbmClickLineItemId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKSITE="ga:dbmClickSite"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMCLICKSITEID="ga:dbmClickSiteId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTADVERTISER="ga:dbmLastEventAdvertiser"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTADVERTISERID="ga:dbmLastEventAdvertiserId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTCREATIVEID="ga:dbmLastEventCreativeId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTEXCHANGE="ga:dbmLastEventExchange"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTEXCHANGEID="ga:dbmLastEventExchangeId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTINSERTIONORDER="ga:dbmLastEventInsertionOrder"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTINSERTIONORDERID="ga:dbmLastEventInsertionOrderId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTLINEITEM="ga:dbmLastEventLineItem"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTLINEITEMID="ga:dbmLastEventLineItemId"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTSITE="ga:dbmLastEventSite"
    API.DOUBLECLICKBIDMANAGER.DIMENSIONS.GA_DBMLASTEVENTSITEID="ga:dbmLastEventSiteId"

    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMCPA="ga:dbmCPA"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMCPC="ga:dbmCPC"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMCPM="ga:dbmCPM"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMCTR="ga:dbmCTR"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMCLICKS="ga:dbmClicks"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMCONVERSIONS="ga:dbmConversions"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMCOST="ga:dbmCost"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMIMPRESSIONS="ga:dbmImpressions"
    API.DOUBLECLICKBIDMANAGER.METRICS.GA_DBMROAS="ga:dbmROAS"


    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSADGROUP="ga:dsAdGroup"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSADGROUPID="ga:dsAdGroupId"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSADVERTISER="ga:dsAdvertiser"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSADVERTISERID="ga:dsAdvertiserId"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSAGENCY="ga:dsAgency"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSAGENCYID="ga:dsAgencyId"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSCAMPAIGN="ga:dsCampaign"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSCAMPAIGNID="ga:dsCampaignId"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSENGINEACCOUNT="ga:dsEngineAccount"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSENGINEACCOUNTID="ga:dsEngineAccountId"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSKEYWORD="ga:dsKeyword"
    API.DOUBLECLICKSEARCH.DIMENSIONS.GA_DSKEYWORDID="ga:dsKeywordId"

    API.DOUBLECLICKSEARCH.METRICS.GA_DSCPC="ga:dsCPC"
    API.DOUBLECLICKSEARCH.METRICS.GA_DSCTR="ga:dsCTR"
    API.DOUBLECLICKSEARCH.METRICS.GA_DSCLICKS="ga:dsClicks"
    API.DOUBLECLICKSEARCH.METRICS.GA_DSCOST="ga:dsCost"
    API.DOUBLECLICKSEARCH.METRICS.GA_DSIMPRESSIONS="ga:dsImpressions"
    API.DOUBLECLICKSEARCH.METRICS.GA_DSPROFIT="ga:dsProfit"
    API.DOUBLECLICKSEARCH.METRICS.GA_DSRETURNONADSPEND="ga:dsReturnOnAdSpend"
    API.DOUBLECLICKSEARCH.METRICS.GA_DSREVENUEPERCLICK="ga:dsRevenuePerClick"